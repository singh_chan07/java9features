Java 9 Features
================

1. Try With Resources
2. Private methods for Interfaces

These are the language change that has been brought by Java in JDK 9.

Try With Resources
------------------
With **Try With Resources** we don't need to explicitly close the Resources we are going to use throught the program,
the try with resources will automatically close resource. But We will only be able to access the withing the try with 
resource block.


Private methods for Interfaces
------------------------------
Java 9 introduced private methods in Interfaces to allow default method to share some data with each other.



Java 10 Enhancements
=====================
1. Type inference of local variables with initializer - It allows us to declare a local variable with ***var*** keyword, 
and compiler will deduce the type automatically from the initializer. But it is not applicable for member variables.
2. New Methods to Unmodifiable Collections.
3. Optional.OrElseThrow





Java 11 Features
=================

1. New ***Collection.toArray(IntFunction)*** Default Method - 
2. Local-Variable Syntax for Lambda Parameters - The reserved type name var can now be used when declaring the formal parameters of a lambda expression
3. The Not Predicate Method - It can be used to negate an existing predicate.
4. New File Methods - Now readString90 and writeString() static methods can be used to read strings from file and write strings to file.
5. New String Methods - the String class now contains isBlank(), lines(), strip(), stripLeading(), stripTrailing(), and repeat() methods.

