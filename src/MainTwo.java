public class MainTwo {

    public static void main(String[] args) {
        MyInterface obj = new MyClass();
        System.out.println(obj.addSquare(4, 5));
        System.out.println(obj.divideSquare(20 , 5));


        obj = new MyClass2();
        System.out.println(obj.addSquare(1, 3));
        System.out.println(obj.divideSquare(1, 0));
    }
}


interface MyInterface{

    private double square(double x){
        return Math.pow(x, 2);
    }
    default double addSquare(double x, double y){
        return square(x) + square(y);
    }
    default double divideSquare(double x, double y){
        return square(x)/square(y);
    }
}

class MyClass2 implements MyInterface{

}


class MyClass implements MyInterface{


    @Override
    public double divideSquare(double x, double y) {
        try{
            double res = x/y;
            return  res;
        }catch (ArithmeticException ie){
            System.out.println("DIvide by zero");
        }
        return Double.POSITIVE_INFINITY;
    }
}
