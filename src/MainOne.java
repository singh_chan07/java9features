import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MainOne {



    // Demonstration of try with resources.
    public static void main(String[] args) throws IOException {
        noTryWithResources();


        tryWithResources();

    }



    /*
    Without try with resources we will use try catch block and we have to manually close the resource.
     */

    public static void noTryWithResources() throws IOException {
        BufferedReader br = null;

        try{
            //try to access the file
            br = new BufferedReader(new FileReader(".\\repository\\us-500.csv"));
            //output the the line read
            System.out.println(br.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // But if we try to access the file outside the try block, we will easily access because
        //It is not closed and We need explicitly close it

        System.out.println(br.readLine());


        //Here we have to explicitly close the resource
        br.close();

        try {
            br.readLine();
        } catch (IOException e) {
            System.out.println("Thrown after the resource is closed");
        }
    }

    /*
    With try with resources we don't need to explicitly close the connection, it will be automatically
    close for us, but we cannot acces the resource outside the try block
     */
    public static void tryWithResources(){



        try(BufferedReader br = new BufferedReader(new FileReader(".\\repository\\us-500.csv")) ){

            System.out.println(br.readLine());

        }catch (FileNotFoundException f){
            System.out.println("FIle not found");
        }catch (IOException ie){
            System.out.println(ie.getMessage());
        }


    }
}
