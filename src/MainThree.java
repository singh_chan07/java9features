import java.util.List;
import java.util.stream.Collectors;

public class MainThree {

    public static void main(String[] args) {

        //isBlank() method
        String str = "kdkdjkd";
        System.out.println(str.isBlank());

        str = "";
        System.out.println(str.isBlank());


        //lines method return a stream which allows to perform various operations on it
        str = "this is some random string";
        List<String> strings = str.lines().collect(Collectors.toList());

        for(String s: strings){
            System.out.println(s);
        }


        //Repeat concatenate the itself the times which are specified
        str = "this is a string";
        System.out.println(str.repeat(4));


        //StripLeading and stripTrailing will remove blanks from start and end respectively
        str = "    this     is s string   some rand   ";
        System.out.println(str.stripLeading());

        System.out.println(str.stripTrailing());

        //the both methods can be used at once with strip

        System.out.println(str.strip());


    }
}
